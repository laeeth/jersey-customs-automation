from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from webdriver_manager.firefox import GeckoDriverManager
import time
import os
import argparse
#from webdriver_manager.chrome import ChromeDriverManager

def loginCustoms(browser,userID,password):
    browser.get("https://www.customs.gst.gov.je/Login.aspx")
    login = browser.find_element(By.ID, "M_MCPH_Login")
    login.send_keys(userID)
    pw = browser.find_element(By.ID, "M_MCPH_Password")
    pw.send_keys(password)
    submit = browser.find_element(By.ID, "M_MCPH_ctl06_B")
    submit.click()
    time.sleep(2)
    assert(browser.title == "Statement of Account")

def addConsignment(browser,consignmentID):
    CLC = browser.find_element(By.ID,"M_MCPH_ConsignmentClc2")
    CLC.send_keys(consignmentID)
    add_button = browser.find_element(By.ID,"M_MCPH_ctl12_B")
    add_button.click()
    time.sleep(2)
    assert(browser.title == "Consignment Details")

# def getConsignmentInfo(browser):
# def declareConsignment(browser,consignmentID):

parser = argparse.ArgumentParser("jersey-customs")
parser.add_argument("--consignment_num", help="Consignment Number to declare",default = os.environ['JERSEY_CONSIGNMENT_TEST'])
args = parser.parse_args()
consignmentNum = args.consignment_num

#browser = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
browser = webdriver.Firefox(service=Service(GeckoDriverManager().install()))
browser.maximize_window()
loginCustoms(browser,os.environ['JERSEY_CUSTOMS_USER'],os.environ['JERSEY_CUSTOMS_PASS'])
addConsignment(browser,consignmentNum)
print(browser.title)
time.sleep(5)
#browser.quit()

